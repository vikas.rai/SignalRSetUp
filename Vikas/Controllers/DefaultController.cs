﻿using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Vikas.Models;

namespace Vikas.Controllers
{
    [RoutePrefix("Chat")]
    public class DefaultController : Controller
    {
        // GET: Default
        [Route("Login")]
        public ActionResult Index()
        {
            var user = new User();
            user.Name = "vikas rai";
            user.Country = 1;
            user.Mobile = "9169201118";
            return View(user);
        }
        [Route("Login")]
        [HttpPost]
        public ActionResult Index(User user)
        {
            if (ModelState.IsValid)
            {
                List<User> Users = new List<User>();

                if (HttpContext.Application["Users"] != null && HttpContext.Application["Users"] is List<User>)
                {
                    Users = (List<User>)HttpContext.Application["Users"];
                }
                if (Users.Where(x => x.Name == user.Name).ToList().Count > 0)
                {
                    //ModelState.AddModelError(string.Empty, "user already exists.");

                }
                else
                {
                    user.lstGroup.Add(new Group() { ID = 1, Name = "General" });
                    int maxUserID = Users != null && Users.Count > 0 ? Users.Max(x => x.ID) : 0;
                    user.ID = maxUserID + 1;

                    if (user.ID == 1 || user.ID == 2)
                        user.lstGroup.Add(new Group() { ID = 2, Name = "General11" });
                    Users.Add(user);
                    Session["user"] = user;
                    HttpContext.Application["Users"] = Users;
                }
                return RedirectToAction("Home");

            }
            return View(user);
        }

        public ActionResult Home()
        {
            if (Session["user"] == null || !(Session["user"] is User))
            {
                return RedirectToAction("Index");
            }
            var user = Session["user"] as User;

            var userModal = new UserModal();
            List<User> Users = new List<User>();
            List<Group> lstGroup = new List<Group>();

            if (HttpContext.Application["Users"] != null && HttpContext.Application["Users"] is List<User>)
            {
                Users = (List<User>)HttpContext.Application["Users"];
            }
            if (HttpContext.Application["lstGroup"] != null && HttpContext.Application["lstGroup"] is List<Group>)
            {
                lstGroup = (List<Group>)HttpContext.Application["lstGroup"];
            }
            userModal.lstGroup = lstGroup;
            userModal.lstUser = Users;
            ViewBag.UserID = user.ID;
            return View(userModal);
        }
        public ActionResult GetChat(int GroupID)
        {
            var lstChat = new List<ChatMessage>();
            if (HttpContext.Application["lstChat"] != null && HttpContext.Application["lstChat"] is List<ChatMessage>)
            {
                lstChat = (List<ChatMessage>)HttpContext.Application["Users"];
            }
            var Result = lstChat.Where(x => x.To == GroupID).ToList();
            return Json(Result);
        }
        public ActionResult Addgroup(string name, List<int> userIDs)
        {
            var lstUser = new List<User>();
            List<Group> lstGroup = new List<Group>();
            if (HttpContext.Application["Users"] != null && HttpContext.Application["Users"] is List<User>)
            {
                lstUser = (List<User>)HttpContext.Application["Users"];
            }
            if (HttpContext.Application["lstGroup"] != null && HttpContext.Application["lstGroup"] is List<Group>)
            {
                lstGroup = (List<Group>)HttpContext.Application["lstGroup"];
            }
            int maxGrpID = lstGroup != null ? lstGroup.Max(x => x.ID) : 1;

            var grp = new Group();
            grp.ID = maxGrpID + 1;
            grp.Name = name;
            lstGroup.Add(grp);
            var uu = lstUser.Where(x => userIDs.Contains(x.ID)).ToList();
            uu.ForEach(x => x.lstGroup.Add(grp));
            AddGroup(grp, uu);
            return Json(true);
        }
        public ActionResult Send(ChatMessage Chat)
        {
            if (Session["user"] == null || !(Session["user"] is User))
            {
                return RedirectToAction("Index");
            }
            var user = Session["user"] as User;

            if (Chat != null)
            {
                Chat.From = Chat.From = user.ID;
                BroadCast("chat", Chat);
            }

            var lstChat = new List<ChatMessage>();
            if (HttpContext.Application["lstChat"] != null && HttpContext.Application["lstChat"] is List<ChatMessage>)
            {
                lstChat = (List<ChatMessage>)HttpContext.Application["lstChat"];
            }
            lstChat.Add(Chat);
            HttpContext.Application["lstChat"] = lstChat;
            return Content("");
        }
        public ActionResult RenderChat(int UserID)
        {
            var user = Session["user"] as User;

            var lstChat = new List<ChatMessage>();
            if (HttpContext.Application["lstChat"] != null && HttpContext.Application["lstChat"] is List<ChatMessage>)
            {
                lstChat = (List<ChatMessage>)HttpContext.Application["lstChat"];
            }
            var Result = lstChat.Where(x => (x.To == user.ID && x.From == UserID) || (x.To == UserID && x.From == user.ID)).ToList();

            ViewBag.UserID = UserID;
            return PartialView(Result);
        }
        private List<ChatMessage> GetChats(int GroupID)
        {
            var lstChat = new List<ChatMessage>();
            if (HttpContext.Application["lstChat"] != null && HttpContext.Application["lstChat"] is List<ChatMessage>)
            {
                lstChat = (List<ChatMessage>)HttpContext.Application["lstChat"];
            }
            return lstChat.Where(x => x.To == GroupID).ToList();

        }
        private void BroadCast(string Name, ChatMessage Chat)
        {
            List<User> Users = new List<User>();
            if (HttpContext.Application["Users"] != null && HttpContext.Application["Users"] is List<User>)
            {
                Users = (List<User>)HttpContext.Application["Users"];
                var ToUser = Users.FirstOrDefault(x => x.ID == Chat.To);
                var context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
                if (ToUser != null && !Chat.isGroup)
                    context.Clients.Clients(ToUser.ConID).broadcastMessage(Name, JsonConvert.SerializeObject(Chat));
                else if (Chat.isGroup)
                {
                    if (HttpContext.Application["lstGroup"] != null && HttpContext.Application["lstGroup"] is List<Group>)
                    {
                        var lstGroup = (List<Group>)HttpContext.Application["lstGroup"];
                        var Group = lstGroup.FirstOrDefault(x => x.ID == Chat.To).Name;
                        context.Clients.Group(Group).broadcastMessage(Name, JsonConvert.SerializeObject(Chat));
                    }
                }
            }
        }
        private void AddGroup(Group Grp, List<User> lstUser)
        {
            ChatHub hub = new ChatHub();
            var context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            foreach (var item in lstUser)
            {
                foreach (var ConID in item.ConID)
                {
                    context.Groups.Add(ConID, Grp.Name);
                }
            }
            context.Clients.Group(Grp.Name).broadcastMessage(Grp.Name, JsonConvert.SerializeObject(Grp));
        }
    }
}