﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Vikas.Models
{
    public class ChatHub : Hub
    {
        public void Send(string name, string message)
        {
            // Call the broadcastMessage method to update clients.
            Clients.All.broadcastMessage(name, message);
        }
        public bool addGroup(string Name, List<int> userIDs)
        {
            var lstUser = new List<User>();
            List<Group> lstGroup = new List<Group>();
            if (HttpContext.Current.Application["Users"] != null && HttpContext.Current.Application["Users"] is List<User>)
            {
                lstUser = (List<User>)HttpContext.Current.Application["Users"];
            }
            if (HttpContext.Current.Application["lstGroup"] != null && HttpContext.Current.Application["lstGroup"] is List<Group>)
            {
                lstGroup = (List<Group>)HttpContext.Current.Application["lstGroup"];
            }
            int maxGrpID = lstGroup != null ? lstGroup.Max(x => x.ID) : 1;

            var grp = new Group();
            grp.ID = maxGrpID + 1;
            grp.Name = Name;
            lstGroup.Add(grp);
            var uu = lstUser.Where(x => userIDs.Contains(x.ID)).ToList();
            uu.ForEach(x => x.lstGroup.Add(grp));

            foreach (var item in uu)
            {
                foreach (var ConID in item.ConID)
                {
                    Groups.Add(ConID, grp.Name);
                }
            }
            Clients.Group(grp.Name).addGroup(grp.Name, grp);
            return true;
        }
        public override System.Threading.Tasks.Task OnConnected()
        {
            List<User> Users = new List<User>();
            if (HttpContext.Current.Application["Users"] != null && HttpContext.Current.Application["Users"] is List<User>)
            {
                Users = (List<User>)HttpContext.Current.Application["Users"];
                int userID = Convert.ToInt32(Context.QueryString["userID"]);
                var CurrentUser = Users.SingleOrDefault(x => x.ID == userID);
                HttpContext.Current.Application["Users"] = Users;
                CurrentUser.ConID.Add(Context.ConnectionId);
                var Chat = new ChatModal();

                List<Group> lstGroup = new List<Group>();
                if (HttpContext.Current.Application["lstGroup"] != null && HttpContext.Current.Application["lstGroup"] is List<Group>)
                {
                    lstGroup = (List<Group>)HttpContext.Current.Application["lstGroup"];
                }

                foreach (var item in lstGroup)
                {
                    var IsUserInGroup = CurrentUser.lstGroup.SingleOrDefault(x => x.Name == item.Name);
                    if (IsUserInGroup != null)
                        Groups.Add(Context.ConnectionId, item.Name);
                }
            }

            return Clients.All.joined(Context.ConnectionId, DateTime.Now.ToString());
        }
    }
}