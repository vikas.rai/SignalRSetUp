﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vikas.Models
{
    public class User
    {
        public User()
        {
            lstCountry = new List<Country>();
            lstGroup = new List<Group>();
            ConID = new List<string>();
            lstCountry.Add(new Country() { ID = 1, Name = "India" });
            lstCountry.Add(new Country() { ID = 2, Name = "Pakistan" });
            lstCountry.Add(new Country() { ID = 3, Name = "America" });
            lstCountry.Add(new Country() { ID = 4, Name = "England" });

        }
        public int ID { get; set; }
        public string Name { get; set; }
        public string Mobile { get; set; }
        public int Country { get; set; }

        public List<string> ConID { get; set; }

        public List<Group> lstGroup { get; set; }

        public List<Country> lstCountry { get; set; }
    }

    public class Country
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
    public class ChatMessage
    {
        public int From { get; set; }
        public int To { get; set; }
        public bool isGroup { get; set; }
        public string Message { get; set; }
        public DateTime Date { get; set; }
    }
    public class Group
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }

    public class ChatModal
    {
        public ChatModal()
        {
            lstChats = new List<Models.ChatMessage>();
        }
        public List<ChatMessage> lstChats { get; set; }
    }

    public class UserModal
    {
        public UserModal()
        {
            lstUser = new List<Models.User>();
            lstGroup = new List<Models.Group>();
        }
        public List<User> lstUser { get; set; }
        public List<Group> lstGroup { get; set; }
    }
}