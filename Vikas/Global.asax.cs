﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Vikas.Models;

namespace Vikas
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

           var lstGroup = new List<Group>();
            lstGroup.Add(new Group() { ID = 1, Name = "General" });
            lstGroup.Add(new Group() { ID = 2, Name = "General11" });
            Application["lstGroup"] = lstGroup;
        }

        protected void Application_Error()
        {
            HttpContext httpContext = HttpContext.Current;
            if (httpContext != null)
            {
                var parameters = httpContext.Request.Params.ToString();
                var path = httpContext.Request.RawUrl;

                var data = $"{path} \r\n{parameters}";

                if ((MvcHandler)httpContext.CurrentHandler != null)
                {
                    RequestContext requestContext = ((MvcHandler)httpContext.CurrentHandler).RequestContext;


                    /* when the request is ajax the system can automatically handle a mistake with a JSON response. then overwrites the default response */
                    if (requestContext.HttpContext.Request.IsAjaxRequest())
                    {
                        httpContext.Response.Clear();
                        string controllerName = requestContext.RouteData.GetRequiredString("controller");
                        IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();
                        if (controllerName != "signalr")
                        {

                            IController controller = factory.CreateController(requestContext, controllerName);
                            ControllerContext controllerContext = new ControllerContext(requestContext, (ControllerBase)controller);

                            Exception exception = Server.GetLastError();
                        }
                        else
                        {

                        }
                    }
                    else
                    {
                        if (Server.GetLastError() != null)
                        {
                        }
                    }
                }

            }

        }
    }
}
